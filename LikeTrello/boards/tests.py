from django.test import TestCase

from .models import Roster, Card, Board
from .tools import drag

class ToolsTest(TestCase):
    """test for our tidy_drag function"""
    def test_on_cards(self):
        board = Board.objects.create(name='im board')
        roster = Roster.objects.create(name='im roster', position=1, board=board)
        lst = range(1, 11)
        for i in lst:
            Card.objects.create(text='card%s' % i, position=i, roster=roster)

        drag(roster.card_set.all(), Card.objects.get(position=2), 6)
        self.assertIs(Card.objects.get(text='card2').position, 6)
        self.assertIs(Card.objects.get(text='card6').position, 5)







