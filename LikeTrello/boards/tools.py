from django.db.models import F

def drag(lst, obj, drag_here):
    """a function for draging and shuffling our cards and rosters"""
    if obj.position > drag_here:
        lst.filter(position__gte=drag_here).filter(position__lt=obj.position).update(position=F('position') + 1)
        obj.position = drag_here
        obj.save()
    elif obj.position < drag_here:
        lst.filter(position__lte=drag_here).filter(position__gt=obj.position).update(position=F('position') - 1)
        obj.position = drag_here
        obj.save()

def insert(lst, obj):
    lst.filter(position__gte=obj.position).update(position=F('position') + 1)

def update_pos(lst, obj, drag_here):
    if obj.position > drag_here:
        lst.filter(position__gte=drag_here).filter(position__lt=obj.position).update(position=F('position') + 1)
        obj.position = drag_here
    elif obj.position < drag_here:
        lst.filter(position__lte=drag_here).filter(position__gt=obj.position).update(position=F('position') - 1)
        obj.position = drag_here

def pop(lst, obj):
    lst.filter(position__gte=obj.position).update(position=F('position') - 1)
