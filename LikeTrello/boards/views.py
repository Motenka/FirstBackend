from django.shortcuts import get_list_or_404

from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework import generics

from .models import Board, Roster, Card
from .serializers import BoardSerializer, RosterSerializer, CardSerializer
from .tools import drag


class BoardList(generics.ListAPIView):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer


class BoardCreate(generics.CreateAPIView):
    serializer_class = BoardSerializer


class BoardDestroy(generics.DestroyAPIView):
    lookup_url_kwarg = 'board_id'
    queryset = Board.objects.all()
    serializer_class = BoardSerializer


class BoardUpdate(generics.UpdateAPIView):
    lookup_url_kwarg = 'board_id'
    queryset = Board.objects.all()
    serializer_class = BoardSerializer


class BoardRosters(APIView):
    """get a list of rosters for a given board"""
    def get(self, request, board_id):
        rosters = get_list_or_404(Roster, board=board_id)
        serializer = RosterSerializer(rosters, many=True)
        return Response(serializer.data)

class RosterCreate(generics.CreateAPIView):
    serializer_class = RosterSerializer

class RosterUpdate(generics.UpdateAPIView):
    lookup_url_kwarg = 'roster_id'
    queryset = Board.objects.all()
    serializer_class = RosterSerializer

class RosterDestroy(generics.DestroyAPIView):
    lookup_url_kwarg = 'roster_id'
    queryset = Board.objects.all()
    serializer_class = RosterSerializer

class RosterCards(APIView):
    """get a list of cards for a given roster"""

    def get(self, request, roster_id):
        cards = get_list_or_404(Card, roster=roster_id)
        serializer = CardSerializer(cards, many=True)
        return Response(serializer.data)

class CardCreate(generics.CreateAPIView):
    serializer_class = CardSerializer

class CardUpdate(generics.UpdateAPIView):
    lookup_url_kwarg = 'card_id'
    queryset = Roster.objects.all()
    serializer_class = CardSerializer

class CardDestroy(generics.DestroyAPIView):
    lookup_url_kwarg = 'card_id'
    queryset = Roster.objects.all()
    serializer_class = CardSerializer









