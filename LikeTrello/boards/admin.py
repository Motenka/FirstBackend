from django.contrib import admin

from .models import Board, Roster, Card

admin.site.register(Board)
admin.site.register(Roster)
admin.site.register(Card)

