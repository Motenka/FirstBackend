from django.db import models

class TimestampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ['-created_at', '-updated_at']



class Board(TimestampedModel):
    name = models.CharField('Название доски', max_length=255, blank=False)

    class Meta:
        ordering = ['updated_at']

    def __str__(self):
        return self.name

class Roster(TimestampedModel):
    name = models.CharField('Название списка', max_length=255, blank=False)
    position = models.IntegerField('Позиция на доске', blank=False)
    board = models.ForeignKey('Board', on_delete=models.CASCADE)

    class Meta:
        ordering = ['position']

    def __str__(self):
        return self.name

class Card(TimestampedModel):
    text = models.TextField('Текст карточки')
    position = models.IntegerField('Позиция в списке', blank=False)
    roster = models.ForeignKey('Roster', on_delete=models.CASCADE)

    class Meta:
        ordering = ['position']

    def __str__(self):
        return self.text[:7]
