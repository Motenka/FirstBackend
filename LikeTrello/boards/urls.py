from django.conf.urls import url
from . import views


app_name = 'boards'
urlpatterns = [
   url(r'^$', views.BoardList.as_view()),
   url(r'^create$', views.BoardCreate.as_view()),
   url(r'^(?P<board_id>\d+)$', views.BoardRosters.as_view()),
   url(r'^delete/(?P<board_id>\d+)$', views.BoardDestroy.as_view()),
   url(r'^update/(?P<board_id>\d+)$', views.BoardUpdate.as_view()),
   url(r'^\d+/rosters/create$', views.RosterCreate.as_view()),
   url(r'^\d+/rosters/(?P<roster_id>\d+)/update$', views.RosterUpdate.as_view()),
   url(r'^\d+/rosters/(?P<roster_id>\d+)/delete$', views.RosterDestroy.as_view()),
   url(r'^\d+/rosters/(?P<roster_id>\d+)$', views.RosterCards.as_view()),
   url(r'^\d+/rosters/\d+/cards/create$', views.CardCreate.as_view()),
   url(r'^\d+/rosters/\d+/cards/(?P<card_id>\d+)/update$', views.CardUpdate.as_view()),
   url(r'^\d+/rosters/\d+/cards/(?P<card_id>\d+)/delete$', views.CardDestroy.as_view()),
]