# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-11 02:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boards', '0005_auto_20171211_0457'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='position',
            field=models.IntegerField(verbose_name='Позиция в списке'),
        ),
        migrations.AlterField(
            model_name='roster',
            name='position',
            field=models.IntegerField(verbose_name='Позиция на доске'),
        ),
    ]
