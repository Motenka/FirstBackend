from rest_framework import serializers
from .models import Board, Roster, Card
from .tools import insert, update_pos, pop

class BoardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Board
        fields = '__all__'

class RosterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Roster
        fields = '__all__'

    def create(self, validated_data):
        roster = Roster(**validated_data)
        insert(validated_data['board'].roster_set.all(), roster)
        roster.save()
        return roster

    def update(self, obj, validated_data):
        update_pos(validated_data['board'].roster_set.all(), obj, validated_data['position'])
        obj.name = validated_data.get('name', obj.name)
        obj.board = validated_data.get('board', obj.board)
        return obj

    def delete(self, obj, validated_data):
        obj.delete()
        pop(validated_data['board'].roster_set.all(), obj)

class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = '__all__'

    def create(self, validated_data):
        card = Card(**validated_data)
        insert(validated_data['roster'].card_set.all(), card)
        card.save()
        return card

    def update(self, obj, validated_data):
        update_pos(validated_data['roster'].roster_set.all(), obj, validated_data['position'])
        obj.text = validated_data.get('text', obj.text)
        obj.board = validated_data.get('roster', obj.board)
        return obj

    def delete(self, obj, validated_data):
        obj.delete()
        pop(validated_data['roster'].roster_set.all(), obj)